﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CalanceTest
{
    class Program
    {
        static void Main(string[] args)
        {
            // Set the path and file name for location of resulting text file
            // ***RESET FOR ANY USAGE ON A DIFFERENT MACHINE***
            string FILE_PATH = "C:\\PersonalProjects\\api-test\\CSharpMethod\\CSharpMethod\\";
            string FILE_NAME = "commits.txt";
            // Set the api path
            string API_PATH = "https://bitbucket.org/api/2.0/repositories/CalanceUs/api-test/commits";

            // Create the WebRequest object
            WebRequest req = WebRequest.Create(API_PATH);
            req.Method = "GET";

            // Retrieve the http response
            HttpWebResponse resp = null;
            resp = (HttpWebResponse)req.GetResponse();

            // Parse the http response into a string
            string respString;
            Stream stream = resp.GetResponseStream();
            StreamReader sr = new StreamReader(stream);
            respString = sr.ReadToEnd();
            sr.Close();

            // Deserialize the JSON string into a c# object, then retrieve the values variable
            dynamic msgsObject = JsonConvert.DeserializeObject(respString);
            // Grab the values array from msgsObject
            dynamic vals = msgsObject.values;

            // Create a list to store the messages in the vals array, then iterate through vals and add all the messages
            var commitMsgs = new List<string>();
            foreach (dynamic val in vals)
            {
                commitMsgs.Add((string)val.message);
            }

            // Reverse the order of commitMsgs (for chronological order), then iterate over the List and write 
            commitMsgs.Reverse();
            StreamWriter file = new StreamWriter(FILE_PATH + FILE_NAME);
            var iter = commitMsgs.GetEnumerator();
            while (iter.MoveNext())
            {
                Console.WriteLine(iter.Current);
                file.WriteLine(iter.Current);
            }
            file.Close();
        }
    }
}
