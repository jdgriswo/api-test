import requests
import json

# Retrieve info from bitbucket's commits api
resp = requests.get("https://bitbucket.org/api/2.0/repositories/CalanceUs/api-test/commits")
# Convert data into a dict
respObj = json.loads(json.dumps(resp.json()))
# Create a file titled "commits.txt"
file = open("commits.txt", "w+")

# Iterate through "values" array in respObj in reverse (chronological) order,
# and write the "message" value for each to the opened file
for value in reversed(respObj["values"]):
    file.write(value["message"])

# Close the file
file.close()
